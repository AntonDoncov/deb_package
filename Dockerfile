FROM ubuntu:20.04

RUN apt-get update \
    && apt-get install -y fakeroot \
    && apt-get install -y dpkg-dev


ENV PYSETUP_PATH="/opt/pysetup"

WORKDIR $PYSETUP_PATH

COPY ./ ./

RUN chmod 0755 myproject/DEBIAN \
    && ls -ld myproject/DEBIAN \
    && fakeroot dpkg-deb --build myproject

RUN mkdir $PYSETUP_PATH/pyrepo \
    && cp -r $PYSETUP_PATH/myproject.deb $PYSETUP_PATH/pyrepo/ \
    && dpkg-scanpackages . $PYSETUP_PATH/pyrepo | gzip -c9 > $PYSETUP_PATH/pyrepo/Packages.gz \
    && echo "deb [trusted=yes] file://$PYSETUP_PATH/pyrepo ./" | tee -a /etc/apt/sources.list > /dev/null \
    && apt-get update \
    && apt-get -y install myproject \
    && python3 /usr/local/bin/myproject.py

CMD ["bash"]
