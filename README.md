1. Собрать образ:
```
docker build -t python-deb-builder:0.1.0 .
```
2. Запустить контейнер:
```
docker run -it python-deb-builder:0.1.0
```
3. Скопировать deb-пакет из контейнера в рабочую директорию:
```
docker cp <id котейнера>:/opt/pysetup/myproject.deb .
```
4. Установить deb-пакет
```
sudo apt-get install ./myproject.deb
```
5. Запустить установленный скрипт:
```
python3 /usr/local/bin/myproject.py
```
6. Удалить скрипт:
```
sudo apt-get remove myproject
```